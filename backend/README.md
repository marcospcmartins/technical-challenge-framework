# Como utilizar

## Descrição

Desafio técnico da empresa Framework.

Esse desafio foi desenvolvido utilizando o framework nestjs e typescript.

Os conceitos de microserviços e padrão DDD foram levados em consideração para haver facil manutenção e alta performance.

Após iniciar a aplicação, ela estará disponível no endereço <http://localhost:4000/api> para que o frontend possa consumí-lo.

## Arquivo .env

> O arquivo .env está sendo utilizado para fornecer alguns parametros essenciais para a aplicação.

## Instalação

```bash
npm install
```

## Rodando a aplicação backend

```bash
npm run start
```

## Teste

```bash
# Testes unitários
$ npm run test

# Teste de cobertura
$ npm run test:cov
```

## Swagger

Acesse <http://localhost:4000/swagger> após rodar a aplicação.

Rota Health verifica se a api está online.

Rota Calculate recupera os dados calculados através de um parametro inserido na url.
