import { NoParamsAvailableError } from './no-params-available-error';

interface Props {
    number: number;
}

export class Calculate {
    private constructor(private readonly props: Props) {}

    static create(data: Props) {
        if (
            data.number &&
            typeof data.number === 'number' &&
            data.number >= 0
        ) {
            return new Calculate(data);
        }

        throw new NoParamsAvailableError();
    }

    get number(): number {
        return this.props.number;
    }
}
