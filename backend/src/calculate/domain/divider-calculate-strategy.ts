interface Props {
    number: number;
}

export class DividerCalculateStrategy {
    constructor(private readonly props: Props) {}

    calculate(): number[] {
        const dividers = [];
        for (let i = 1; i <= this.props.number; i += 1) {
            const calc = this.props.number % i;
            if (calc === 0) {
                dividers.push(i);
            }
        }
        return dividers;
    }
}
