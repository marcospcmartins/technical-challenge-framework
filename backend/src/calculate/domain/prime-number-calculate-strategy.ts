interface Props {
    dividers: number[];
}

export class PrimeNumberCalculateStrategy {
    constructor(private readonly props: Props) {}

    calculate(): number[] {
        return this.props.dividers.filter((divider) => {
            if (divider !== 1 && this.isPrime(divider)) {
                return divider;
            }
            return null;
        });
    }

    private isPrime(divider: number): boolean {
        for (let i = 2; i < divider; i += 1) {
            const calc = divider % i;
            if (calc === 0) {
                return false;
            }
        }
        return true;
    }
}
