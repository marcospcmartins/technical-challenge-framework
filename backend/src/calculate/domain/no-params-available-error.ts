import { HttpStatus } from '@nestjs/common';
import { DomainError } from '../../commons/domain/domain-error';
import { Env } from '../../commons/environment/env';

export class NoParamsAvailableError extends DomainError {
    constructor() {
        super(
            'Params not available.',
            `${Env.SERVICE_NAME_ID}/no-params-available`,
            HttpStatus.NOT_FOUND,
        );
    }
}
