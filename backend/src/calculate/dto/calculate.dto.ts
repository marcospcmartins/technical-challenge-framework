import { ApiProperty } from '@nestjs/swagger';
import { plainToClass, instanceToInstance } from 'class-transformer';
import { IsArray, IsNotEmpty, IsNumber } from 'class-validator';

export class CalculateDto {
    @ApiProperty({
        description: 'Number',
        type: Number,
        example: 45,
    })
    @IsNotEmpty()
    @IsNumber()
    number: number;

    @ApiProperty({
        description: 'Dividers',
        type: [Number],
        example: [1, 3, 5, 9, 15, 45],
    })
    @IsNotEmpty()
    @IsArray()
    dividers: number[];

    @ApiProperty({
        description: 'Prime numbers',
        type: [Number],
        example: [1, 3, 5],
    })
    @IsNotEmpty()
    @IsArray()
    primeNumbers: number[];

    public static factory(
        resultQuery: CalculateDto | CalculateDto[],
    ): CalculateDto | CalculateDto[] {
        const resultQueryDto = plainToClass(CalculateDto, resultQuery, {
            ignoreDecorators: false,
        });

        return instanceToInstance(resultQueryDto);
    }
}
