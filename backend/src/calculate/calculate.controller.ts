import { Controller, Get, Logger, Param, Response } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CalculateDto } from './dto/calculate.dto';
import { CalculateService } from './calculate.service';

@Controller('calculate')
@ApiTags('Calculate')
export class CalculateController {
    constructor(private readonly calculateService: CalculateService) {}

    @Get(':number')
    @ApiOkResponse({ type: CalculateDto })
    calculate(@Response() res, @Param('number') number: string) {
        Logger.log(`Params ${number} received.`);
        const output = this.calculateService.proccess(Number(number));
        return res.send(CalculateDto.factory(output));
    }
}
