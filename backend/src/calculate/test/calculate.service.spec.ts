import { Test } from '@nestjs/testing';
import { CalculateService } from '../calculate.service';
import { CalculateDto } from '../dto/calculate.dto';

const calculateDtoFixture: CalculateDto = {
    number: 23,
    dividers: [1, 23],
    primeNumbers: [23],
};

describe('Calculate Service', () => {
    let calculateService: CalculateService;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [CalculateService],
        }).compile();

        calculateService = await moduleRef.resolve(CalculateService);
    });

    describe('proccess', () => {
        describe('when proccess is called', () => {
            test('then it should return a object with dividers and prime numbers', async () => {
                const calculate = await calculateService.proccess(23);

                expect(calculate.number).toEqual(calculateDtoFixture.number);
                expect(Array.isArray(calculate.dividers)).toBeTruthy();
                expect(calculate.dividers).toEqual(
                    calculateDtoFixture.dividers,
                );
                expect(Array.isArray(calculate.primeNumbers)).toBeTruthy();
                expect(calculate.primeNumbers).toEqual(
                    calculateDtoFixture.primeNumbers,
                );
            });
        });
    });
});
