import { Test } from '@nestjs/testing';
import { FastifyReply } from 'fastify';
import { CalculateController } from '../calculate.controller';
import { CalculateService } from '../calculate.service';
import { CalculateDto } from '../dto/calculate.dto';

const calculateDtoFixture: CalculateDto = {
    number: 23,
    dividers: [1, 23],
    primeNumbers: [23],
};

class CalculateServiceMocked {
    static proccess = jest.fn().mockResolvedValue(calculateDtoFixture);
}

const response: FastifyReply = {
    ...({} as FastifyReply),
    send: (data) => {
        return data as FastifyReply;
    },
    status: () => {
        return response;
    },
};

describe('Calculate Controller', () => {
    let calculateController: CalculateController;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [CalculateController],
            providers: [
                {
                    provide: CalculateService,
                    useValue: CalculateServiceMocked,
                },
            ],
        }).compile();

        calculateController =
            moduleRef.get<CalculateController>(CalculateController);
    });

    describe('calculate', () => {
        describe('when calculate is called', () => {
            test('then it should return the dividers and prime numbers', async () => {
                expect(
                    await calculateController.calculate(response, '23'),
                ).toEqual(calculateDtoFixture);
            });
        });
    });
});
