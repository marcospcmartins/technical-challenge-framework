import { Injectable } from '@nestjs/common';
import { Calculate } from './domain/calculate';
import { DividerCalculateStrategy } from './domain/divider-calculate-strategy';
import { PrimeNumberCalculateStrategy } from './domain/prime-number-calculate-strategy';

@Injectable()
export class CalculateService {
    proccess(number: number) {
        const numberCreated = Calculate.create({ number });

        const dividerCalculate = new DividerCalculateStrategy({
            number: numberCreated.number,
        });
        const dividers = dividerCalculate.calculate();

        const primeNumberCalculate = new PrimeNumberCalculateStrategy({
            dividers,
        });
        const primeNumbers = primeNumberCalculate.calculate();

        return {
            number,
            dividers,
            primeNumbers,
        };
    }
}
