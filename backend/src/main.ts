import 'reflect-metadata';
import fastifyHelmet from '@fastify/helmet';
import { Logger, RequestMethod, ValidationPipe } from '@nestjs/common';
import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';
import { NestFactory } from '@nestjs/core';
import {
    FastifyAdapter,
    NestFastifyApplication,
} from '@nestjs/platform-fastify';
import {
    DocumentBuilder,
    SwaggerDocumentOptions,
    SwaggerModule,
} from '@nestjs/swagger';
import { AppModule } from './app.module';
import { DefaultExceptionsFilter } from './commons/filters/default-exception.filter';
import { TransformInterceptor } from './commons/interceptors/transform.interceptor';
import { Env } from './commons/environment/env';

async function bootstrap(): Promise<void> {
    const fastifyAdapter = new FastifyAdapter({
        logger: true,
        maxParamLength: 1000,
        bodyLimit: 12485760, // 10Mb
    });

    const app = await NestFactory.create<NestFastifyApplication>(
        AppModule,
        fastifyAdapter,
    );

    app.register(fastifyHelmet, {
        contentSecurityPolicy: {
            directives: {
                defaultSrc: [`'self'`],
                styleSrc: [`'self'`, `'unsafe-inline'`],
                imgSrc: [`'self'`, 'data:', 'validator.swagger.io'],
                scriptSrc: [`'self'`, `https: 'unsafe-inline'`],
            },
        },
    });

    app.register(import('@fastify/compress'), {
        encodings: ['gzip', 'deflate'],
    });

    const corsOptions: CorsOptions = {
        origin: true,
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'OPTIONS'],
        preflightContinue: false,
        optionsSuccessStatus: 200,
    };
    app.enableCors(corsOptions);

    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    app.useGlobalFilters(new DefaultExceptionsFilter());
    app.useGlobalInterceptors(new TransformInterceptor());
    app.setGlobalPrefix(Env.APPLICATION_PREFIX, {
        exclude: [{ path: 'health', method: RequestMethod.GET }],
    });

    const swaggerDocumentBuilder = new DocumentBuilder()
        .addBearerAuth()
        .setTitle(Env.SWAGGER_TITLE)
        .setDescription(Env.SWAGGER_DESCRIPTION)
        .setVersion(Env.APPLICATION_VERSION)
        .addServer(Env.SWAGGER_SERVER)
        .build();
    const swaggerDocumentoptions: SwaggerDocumentOptions = {
        operationIdFactory: (_controllerKey: string, methodKey: string) =>
            methodKey,
    };
    const swaggerDocument = SwaggerModule.createDocument(
        app,
        swaggerDocumentBuilder,
        swaggerDocumentoptions,
    );
    SwaggerModule.setup(Env.SWAGGER_DOCS, app, swaggerDocument);

    await app.listen(Env.APPLICATION_PORT, '0.0.0.0');
    Logger.log(
        `Application is running on: http://localhost:${Env.APPLICATION_PORT}`,
    );
}
bootstrap();
