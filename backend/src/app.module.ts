import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { CalculateModule } from './calculate/calculate.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: '.env',
            isGlobal: true,
            load: [],
        }),
        CalculateModule,
    ],
    controllers: [AppController],
    providers: [],
})
export class AppModule {}
