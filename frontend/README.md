# Como utilizar

## Descrição

> Frontend desenvolvido através do framework Reactjs e TypeScript.

Após iniciar a aplicação, ela estará disponível no endereço <http://localhost:3000>.

### Essa aplicação deve ser inicializada após o backend já está online, para que o frontend possa consumir a api do backend

## Para rodar a aplicação

```bash
yarn install
```

```bash
yarn start
```
