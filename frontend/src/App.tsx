import { AxiosResponse } from 'axios';
import React, { useState } from 'react';
import './App.css';
import api from './services/api';

type Data = {
    number: number;
    dividers: number[];
    primeNumbers: number[];
};

export default function App() {
    const [data, setData] = useState<Data>();
    const [number, setNumber] = useState<any>();
    const [error, setError] = useState<string>('');

    async function calculate() {
        const param = Number(number);
        if (param && typeof param === 'number' && param >= 0) {
            const response: AxiosResponse = await api.get(
                `/calculate/${param}`,
            );
            setData(response.data);
            setError('none');
        } else {
            setError('block');
        }
    }

    async function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
        setData({ number, dividers: [], primeNumbers: [] });
        setNumber(e.target.value);
    }

    return (
        <div className="container">
            <div className="row mt-5 justify-content-center">
                <p className="h3 text-center">
                    Technical Challenge - Framework
                </p>
            </div>

            <div className="row mt-5 justify-content-center">
                <div className="col-3">
                    <div className="input-group mb-3">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Dígite um número"
                            value={number}
                            onChange={handleChange}
                        />
                        <div
                            className="invalid-feedback"
                            style={{ display: error }}
                        >
                            Entrada inválida, verifique!
                        </div>
                    </div>
                </div>
            </div>
            <div className="row justify-content-center">
                <div className="col-3">
                    <div className="d-grid gap-2">
                        <button
                            className="btn btn-primary"
                            type="button"
                            onClick={calculate}
                        >
                            Calcular
                        </button>
                    </div>
                </div>
            </div>
            <div className="row mt-5 justify-content-center form-control">
                <label className="text-center mb-3">Número escolhido</label>
                <p className="h5 text-center">{number}</p>
            </div>
            <div className="row mt-5 justify-content-center form-control">
                <label className="text-center mb-3">Divisores</label>
                <p className="h5 text-center">
                    {data?.dividers ? data.dividers.join(', ') : null}
                </p>
            </div>
            <div className="row mt-5 justify-content-center form-control">
                <label className="text-center mb-3">Números primos</label>
                <p className="h5 text-center">
                    {data?.primeNumbers ? data.primeNumbers.join(', ') : null}
                </p>
            </div>
        </div>
    );
}
